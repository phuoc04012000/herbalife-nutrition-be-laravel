<?php

use Illuminate\Support\Facades\Route;

// Route::get('/users', function () {
//     return response()->json('ok roi');
// });
Route::middleware(["auth:sanctum"])->get('/users/detail', [\App\Http\Controllers\Apis\Users\UserController::class, 'show']);
Route::post('/login', [\App\Http\Controllers\Auth\AuthController::class, 'login']);
Route::post('/register', [\App\Http\Controllers\Auth\AuthController::class, 'register']);
Route::middleware(["auth:sanctum"])->post('/logout', [\App\Http\Controllers\Auth\AuthController::class, 'logout']);
