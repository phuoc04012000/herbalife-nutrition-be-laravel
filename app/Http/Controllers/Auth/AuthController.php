<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function register(\App\Http\Requests\Auth\RegisterRequest$request): \Illuminate\Http\Response | JsonResponse | \Illuminate\Contracts\Foundation\Application | \Illuminate\Contracts\Routing\ResponseFactory
    {
        DB::beginTransaction();

        try {
            $fields = $request->only(['name', 'email', 'password']);
            $fields['password'] = bcrypt($fields['password']);

            $userProfile = $request->only(['phone', 'address']);

            $user = User::create($fields);
            $user->profile()->create($userProfile);

            DB::commit();

            $message = 'Register successfully';
            return response()->json(['success' => $message], 201);
        } catch (Exception $e) {
            DB::rollBack();
            $message = $e->getMessage();
        }

        return response($message);
    }

    public function login(\App\Http\Requests\Auth\LoginRequest$request): JsonResponse
    {
        // check email
        $user = User::where('email', $request->email)->first();

        // check password
        if (!$user || !Hash::check($request->password, $user->password)) {
            return response()->json('Email or password is not correct!', 401);
        }
        $token = $user->createToken('myToken')->plainTextToken;
        $response = [
            'token' => $token,
            'user' => $user,
        ];
        return response()->json($response, 201);
    }

    public function logout(): JsonResponse
    {
        auth()->user()->tokens()->delete();

        return response()->json([
            'message' => 'Logged out',
        ]);
    }
}
