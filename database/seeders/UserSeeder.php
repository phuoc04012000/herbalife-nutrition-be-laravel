<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'email' => 'phuoc04012000@gmail.com',
            'password' => bcrypt('ngocphuocha'),
            'role_id' => 2,
            'name'  => 'Tran Ngoc Phuoc'
        ]);
    }
}
